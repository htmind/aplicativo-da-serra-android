package br.com.htmind.aplicativodaserra.rest;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.htmind.aplicativodaserra.MainActivity;
import br.com.htmind.aplicativodaserra.R;
import br.com.htmind.aplicativodaserra.model.Carro;

public class JsonAppEngineTask extends AsyncTask<String, Void, List<Carro>>{

    private final MainActivity _activity;
    private ProgressDialog _dialog;
    public JsonAppEngineTask(MainActivity activity){

        _activity = activity;
    }
    @Override
    protected List<Carro> doInBackground(String... strings) {
        String url = strings[0];
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        try {
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if(entity != null){
                InputStream inputStream = entity.getContent();
                String json = getInputStreamString(inputStream);
                return parseJsonToCarro(json);
            }
        } catch (Exception e) {
            Log.e("Aplicativo da Serra", "erro", e);
        }

        return null;
    }

    private List<Carro> parseJsonToCarro(String json) {
        List<Carro> carros = new ArrayList<Carro>();
        try{
            JSONArray array = new JSONArray(json);
            for (int i = 0; i< array.length(); i++){
                JSONObject carroJsonObject =  array.getJSONObject(i);
                Carro carro = new Carro();
                carro.setId(Integer.parseInt(carroJsonObject.getString("id")));
                carro.setModelo(carroJsonObject.getString("modelo"));
                carro.setMarca(carroJsonObject.getString("marca"));
                carro.setUrl(carroJsonObject.getString("url"));
                JSONArray fotos = carroJsonObject.getJSONArray("fotos");
                List<String> fotosStrings = new ArrayList<String>();
                for (int j = 0; j < fotos.length(); j++){
                    fotosStrings.add(fotos.getString(j));
                }
                fotosStrings.toArray();
                if(!fotosStrings.isEmpty())
                    carro.setFotos(fotosStrings);
                carros.add(carro);
            }
        } catch (Exception e){
            Log.e(_activity.getString(R.string.strProcurando), _activity.getString(R.string.strAguarde), e);
        }
        return carros;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        _dialog = ProgressDialog.show(_activity, _activity.getString(R.string.strProcurando), _activity.getString(R.string.strAguarde));
    }

    @Override
    protected void onPostExecute(List<Carro> objects) {
        super.onPostExecute(objects);
        _dialog.cancel();
        ListView listView = (ListView)_activity.findViewById(R.id.listView);
        ArrayAdapter<Carro> carroArrayAdapter = new ArrayAdapter<Carro>(_activity, android.R.layout.simple_list_item_1, objects);
        listView.setAdapter(carroArrayAdapter);
    }

    private String getInputStreamString(InputStream is) throws IOException {
        byte[] bytes = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int lidos;
        while ((lidos = is.read(bytes)) > 0) {
            baos.write(bytes, 0, lidos);
        }
        return new String(baos.toByteArray());
    }
}
