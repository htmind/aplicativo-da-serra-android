package br.com.htmind.aplicativodaserra;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import br.com.htmind.aplicativodaserra.model.Carro;
import br.com.htmind.aplicativodaserra.rest.JsonAppEngineTask;


public class MainActivity extends Activity {


    final String urlAplicativoDaSerraAppEngine = "http://aplicativodaserra.appspot.com/carros?modelo=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button button = (Button)findViewById(R.id.searchButton);
        final AlertDialog alert = new AlertDialog.Builder(this).create();
        final ListView listView = (ListView)findViewById(R.id.listView);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EditText editText = (EditText)findViewById(R.id.editTextMainSearch);
                if(editText.getText().toString().equals("")){
                    return;
                }
                new JsonAppEngineTask(MainActivity.this).execute(String.format(urlAplicativoDaSerraAppEngine + editText.getText().toString()));
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Carro carro = (Carro)listView.getItemAtPosition(i);
                Intent intent = new Intent(getBaseContext(), DetailsActivity.class);
                intent.putExtra("carro", carro);
                startActivity(intent);
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
