package br.com.htmind.aplicativodaserra;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;

import br.com.htmind.aplicativodaserra.model.Carro;
import br.com.htmind.aplicativodaserra.rest.DownloadImageTask;

public class DetailsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Carro carro = (Carro)getIntent().getExtras().getSerializable("carro");
        setTitle(carro.toString());
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id._linearLayout);
        for(String foto: carro.getFotos()){
            View inflate = getLayoutInflater().inflate(R.layout.cell, null);
            final ImageView _image = (ImageView) inflate.findViewById(R.id._image);
            new DownloadImageTask(_image).execute(foto);
            linearLayout.addView(inflate);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.details, menu);
        return true;
    }
    
}
